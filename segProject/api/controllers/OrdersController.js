/**
 * OrdersController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    version: function (req, res) {

        return res.json({
            version: req.query.id
        });
    },

    aggregation: function (req,res) {

        Orders.native(function(err, collection) {
            if (err) return res.serverError(err);

            collection.aggregate([ { $group: {_id: { userName: "$userName", date: "$date"}, count: { $sum: 1 } } }]).toArray(function (err, results) {
                if (err) return res.serverError(err);
                console.log('->',results);
                return res.ok(results);
            });
        });
    },

    aggregationByUserId: function (req,res) {

        Orders.native(function(err, collection) {
            if (err) return res.serverError(err);

            collection.aggregate([ { $match: {userId: req.query.userId}}, { $group: {_id: { userName: "$userName", date: "$date"}, count: { $sum: 1 } } }]).toArray(function (err, results) {
                if (err) return res.serverError(err);
                console.log('->',results);
                return res.ok(results);
            });
        });
    }
};
